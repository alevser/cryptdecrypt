import sys
import os
from PyQt5.QtWidgets import (
    QApplication,
    QWidget,
    QVBoxLayout,
    QPushButton,
    QFileDialog,
    QLabel,
    QLineEdit,
    QMessageBox,
)
from Crypto.Cipher import Blowfish
from Crypto.Random import get_random_bytes



class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        layout = QVBoxLayout()

        encrypt_button = QPushButton("Зашифровать")
        encrypt_button.clicked.connect(self.open_encrypt_window)
        decrypt_button = QPushButton("Расшифровать")
        decrypt_button.clicked.connect(self.open_decrypt_window)

        layout.addWidget(encrypt_button)
        layout.addWidget(decrypt_button)

        self.setLayout(layout)
        self.setWindowTitle("Программа криптографической защиты информации")
        self.setGeometry(100, 100, 400, 150)

    def open_encrypt_window(self):
        self.encrypt_window = EncryptWindow()
        self.encrypt_window.show()

    def open_decrypt_window(self):
        self.decrypt_window = DecryptWindow()
        self.decrypt_window.show()


class EncryptWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        layout = QVBoxLayout()

        self.file_label = QLabel("Выберите файл для шифрования:")
        self.file_edit = QLineEdit()
        self.browse_button = QPushButton("Выбрать")
        self.browse_button.clicked.connect(self.browse_file)

        self.encrypt_button = QPushButton("Зашифровать")
        self.encrypt_button.clicked.connect(self.encrypt_file)

        layout.addWidget(self.file_label)
        layout.addWidget(self.file_edit)
        layout.addWidget(self.browse_button)
        layout.addWidget(self.encrypt_button)

        self.setLayout(layout)
        self.setWindowTitle("Шифрование")

    def browse_file(self):
        options = QFileDialog.Options()
        file_name, _ = QFileDialog.getOpenFileName(
            self, "Открыть файл", "", "All Files (*)", options=options
        )
        self.file_edit.setText(file_name)

    def encrypt_file(self):
        input_file = self.file_edit.text()
        if not input_file:
            QMessageBox.warning(
                self, "Внимание!", "Пожалуйста, Выберите файл для шифрования"
            )
            return

        key = get_random_bytes(16)  # Generate a random 128-bit key
        iv = get_random_bytes(8)  # Generate a random 64-bit IV
        cipher = Blowfish.new(key, Blowfish.MODE_CBC, iv)

        with open(input_file, "rb") as file:
            plaintext = file.read()

        # Pad the plaintext to make it a multiple of 8 bytes
        plaintext += b"\0" * (8 - len(plaintext) % 8)

        ciphertext = cipher.encrypt(plaintext)

        encrypted_file = input_file + ".enc"
        with open(encrypted_file, "wb") as file:
            file.write(ciphertext)

        key_file = "encryption_key"
        with open(key_file, "wb") as file:
            file.write(key)
            
        iv_file = 'iv_file'
        with open(iv_file, "wb") as file:
            file.write(iv)

        QMessageBox.information(
            self,
            "Успешно",
            f"Файл зашифрован и сохранен в {encrypted_file}.\nКлюч шифрования сохранен в {key_file}.\nВектор инициализации сохранен в {iv_file}.",
        )


class DecryptWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        layout = QVBoxLayout()

        self.file_label = QLabel("Выберите файл для расшифровки: ")
        self.file_edit = QLineEdit()
        self.browse_button = QPushButton("Выбрать")
        self.browse_button.clicked.connect(self.browse_file)

        self.key_label = QLabel("Выберите ключ шифрования: ")
        self.key_edit = QLineEdit()
        self.key_button = QPushButton("Выбрать")
        self.key_button.clicked.connect(self.browse_key)

        self.iv_label = QLabel("Выберите вектор инициализации: ")
        self.iv_edit = QLineEdit()
        self.iv_button = QPushButton("Выбрать")
        self.iv_button.clicked.connect(self.browse_iv)


        self.decrypt_button = QPushButton("Расшифровать")
        self.decrypt_button.clicked.connect(self.decrypt_file)

        layout.addWidget(self.file_label)
        layout.addWidget(self.file_edit)
        layout.addWidget(self.browse_button)
        layout.addWidget(self.key_label)
        layout.addWidget(self.key_edit)
        layout.addWidget(self.key_button)
        layout.addWidget(self.iv_label)
        layout.addWidget(self.iv_edit)
        layout.addWidget(self.iv_button)
        layout.addWidget(self.decrypt_button)

        self.setLayout(layout)
        self.setWindowTitle("Расшифровка файла")

    def browse_file(self):
        options = QFileDialog.Options()
        file_name, _ = QFileDialog.getOpenFileName(
            self, "Открыть файл", "", "All Files (*)", options=options
        )
        self.file_edit.setText(file_name)

    def browse_key(self):
        options = QFileDialog.Options()
        key_file, _ = QFileDialog.getOpenFileName(
            self, "Открыть ключ", "", "All Files (*)", options=options
        )
        self.key_edit.setText(key_file)
        
    def browse_iv(self):
        options = QFileDialog.Options()
        iv_file, _ = QFileDialog.getOpenFileName(
            self, "Открыть вектор", "", "All Files (*)", options=options
        )
        self.iv_edit.setText(iv_file)

    def decrypt_file(self):
        input_file = self.file_edit.text()
        key_file = self.key_edit.text()
        iv_file = self.iv_edit.text()

        if not input_file or not key_file or not iv_file:
            QMessageBox.warning(
                self,
                "Внимание",
                "Пожалуйста, выберите файл, ключ для расшифровки и вектор!",
            )
            return

        with open(key_file, "rb") as file:
            key = file.read()
            
        with open(iv_file, "rb") as file:
            iv = file.read()
            

        cipher = Blowfish.new(key, Blowfish.MODE_CBC, iv)

        with open(input_file, "rb") as file:
            ciphertext = file.read()

        decrypted_text = cipher.decrypt(ciphertext)
        decrypted_file = os.path.splitext(input_file)[0]  # Remove the ".enc" extension
        decrypted_file += "Decrypted"
        with open(decrypted_file, "wb") as file:
            file.write(decrypted_text)

        QMessageBox.information(
            self, "Успешно", f"Файл расшифрован и сохранен в {decrypted_file}."
        )



app = QApplication(sys.argv)
main_window = MainWindow()
main_window.show()
sys.exit(app.exec_())

